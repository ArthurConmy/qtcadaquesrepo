import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12

ApplicationWindow {
    visible: true
    width: 1000
    height: 700
    title: qsTr("Bananagrams Player")
    id: root

    Image {
        id: background
        source: "images/cawfee.jpg"
        anchors.fill: parent
    }

    // the following array contains the 144 letters in a game of bananagrams
    // we'll shuffle it in order to play the game

    property var lettersArrayPreShuffle: ['J', 'J', 'K', 'K', 'Q', 'Q', 'X', 'X', 'Z', 'Z', 'B', 'B', 'B', 'C', 'C', 'C', 'F', 'F', 'F', 'H', 'H', 'H', 'M', 'M', 'M', 'P', 'P', 'P', 'V', 'V', 'V', 'W', 'W', 'W', 'Y', 'Y', 'Y', 'G', 'G', 'G', 'G', 'L', 'L', 'L', 'L', 'L', 'D', 'D', 'D', 'D', 'D', 'D', 'S', 'S', 'S', 'S', 'S', 'S', 'U', 'U', 'U', 'U', 'U', 'U', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'];

    function shuffle(array) { // see this SO answer for this function: https://stackoverflow.com/a/2450976
                              // simply shuffles the array
      var currentIndex = array.length, temporaryValue, randomIndex;

      // While there remain elements to shuffle...
      while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
      }

      return array;
    }

    property var lettersArray: shuffle(lettersArrayPreShuffle); // not happy with the way this is coded,
                                                                // as for example the lettersArrayPreShuffle
                                                                // is also shuffled : (

    ListModel {
        id: currentLettersModel

        ListElement { number: 0 }
        ListElement { number: 1 }
        ListElement { number: 2 }
        ListElement { number: 3 }
        ListElement { number: 4 }
        ListElement { number: 5 }
        ListElement { number: 6 }
        ListElement { number: 7 }
        ListElement { number: 8 }
        ListElement { number: 9 }
        ListElement { number: 10 }
        ListElement { number: 11 }
        ListElement { number: 12 }
        ListElement { number: 13 }
        ListElement { number: 14 }
        ListElement { number: 15 }
        ListElement { number: 16 }
        ListElement { number: 17 }
        ListElement { number: 18 }
        ListElement { number: 19 }
        ListElement { number: 20 } // hacky code for now, sort this out later
    }

    PeelButton {

        id: thePeelButton

        property int count: 21

        states: State {
            name: "finished"; when: count == lettersArray.length - 1

        }

        MouseArea {
            anchors.fill: parent

            onClicked: {
                thePeelButton.count == lettersArray.length-1 ? thePeelButton.state="finished" : thePeelButton.count++
                thePeelButton.state=="finished" ? console.log("You've finished") : currentLettersModel.append({"number": thePeelButton.count});
            }
        }
    }

    Repeater {
        id: initialLetters

        anchors.centerIn: parent

        width: 450
        height: 450 // with a Grid type, I could simply set rows=3. columns=7
        // had to use this workaround here

        anchors.margins: 20

        //cellWidth: 60
        //cellHeight: 60

        //interactive: false // scrolling is essentially undesirable for the moment
        // maybe this means GridView is inappropriate here though?

        model: currentLettersModel
        delegate: letterDelegate
    }

    Component {
        id: letterDelegate

        Tile {
            id: tiley

            x: index<21 ? 200 + 60*(index%7) : Math.random() * (root.width - 400) + 200
            y: index<21 ? 200 + 60*Math.floor(index/7) : Math.random() * (root.height - 400) + 200 // needs documenting !!!

            letter: lettersArray[index]

            PropertyAnimation {
                target: tiley; // all this will need documenting later
                property: "x";
                from: Math.floor(2*Math.random())==0 ? 0.0 : root.width;
                to: index<21 ? 200 + 60*(index%7) : Math.random() * (root.width - 400) + 200;
                duration: index<21 ? 0 : 450;
                running: true;
                loops: 1;
            }
        }
    }

    Item { // I can't set the focus of the window to true so need to create
           // an item in order to have this functionality
        focus: true
        Keys.onEscapePressed: Qt.quit();
//        Keys.onDigit0Pressed: {

//            for(var i=0; i<lettersArray.length; ++i) {
//                console.log(lettersArray[i]);
//            }
        //}
    }
}
