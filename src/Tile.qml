import QtQuick 2.12

Rectangle {

    // graphics things starting:

    width: 50
    height: 50

    gradient: Gradient {
        GradientStop { position: 0.0; color: "#ffffcc" }
        GradientStop { position: 1.0; color: "#ffff99" }
    }

    border.width: 4
    border.color: color
    radius: 5

    // graphic things finished

    MouseArea {
        anchors.fill: parent
        drag.target: parent

        onClicked : {
            tileLetter.visible = true
        }
    }
    property alias letter:tileLetter.text // unsure where I should place this
                                          // line of code. Possibly in the text
                                          // item below

    Text {
        id: tileLetter
        anchors.centerIn: parent
        antialiasing: true // unsure whether this is actually improving things (letters are a little unclear)

        visible: false

        text: "A" // placeholder before this gets overwritten

        font.pixelSize: 0.8 * parent.height
        font.family: "Ubuntu"
    }
}
