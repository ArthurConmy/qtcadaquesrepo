import QtQuick 2.12
import QtQuick.Controls 2.12

RoundButton {
    width: 100
    height: 100
    icon.color: "black"

    Text {
        anchors.centerIn: parent
        text: "PEEL!"
        font.pixelSize: 30
    }
    anchors.bottom: parent.bottom
    anchors.right: parent.right
    anchors.margins: 50
}
